**ITSM Quickstart**

This quickstart is used in conjunction with the Cadalys Service Management™ and Cadalys Concierge™ products to reduce set up time for new customers. 
---

## Install Directions

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Set up Demo Org
2. Enable Entitlements
3. Enable Communities
4. Enable Lightning Knowledge & Validation Status
5. Enable Paths
6. Enable Translation workbench
	a. Add English
7. Enable Apex settings (when package is still unapproved)
8. Install Concierge
9. Install ITSM
10. Update Sharing Model
11. Set up E2C
12. Turn on OmniChannel
13. Update Support Settings
	a. Create Help Desk Queue
		i. Case
		ii. Working Time
		iii. Self-Reported Working Time
		iv. Help Desk Public Group
	b. Change Automated Case User to system
14. Push Quick Start Package
	a. Ensure you select all Changes and New if you are using Gearset when you are trying to deploy. 
	b. Ensure you are using Filter All for metadata
15. (Optional) Set up Auto-response
16. Activate Lightning Pages for Apps 


---

## Want to contribute?

*Reach out to Josh Saylor to help begin working on additional items to include within our ITSM Quickstart*

---
